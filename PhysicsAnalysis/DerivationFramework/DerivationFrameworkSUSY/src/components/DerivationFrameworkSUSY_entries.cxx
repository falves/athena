#include "DerivationFrameworkSUSY/Truth3CollectionMaker.h"
#include "DerivationFrameworkSUSY/SUSYSignalTagger.h"
#include "DerivationFrameworkSUSY/SUSYIDWeight.h"
#include "DerivationFrameworkSUSY/SUSYGenFilterTool.h"
#include "DerivationFrameworkSUSY/trackIsolationDecorator.h"

using namespace DerivationFramework;

DECLARE_COMPONENT( Truth3CollectionMaker )
DECLARE_COMPONENT( SUSYSignalTagger )
DECLARE_COMPONENT( SUSYIDWeight )
DECLARE_COMPONENT( SUSYGenFilterTool )
DECLARE_COMPONENT( trackIsolationDecorator )

