// AsgExampleTools_entries.cxx

#include <FTagAnalysisAlgorithms/BTaggingEfficiencyAlg.h>
#include <FTagAnalysisAlgorithms/BTaggingInformationDecoratorAlg.h>
#include <FTagAnalysisAlgorithms/BTaggingScoresAlg.h>

DECLARE_COMPONENT (CP::BTaggingEfficiencyAlg)
DECLARE_COMPONENT (CP::BTaggingInformationDecoratorAlg)
DECLARE_COMPONENT (CP::BTaggingScoresAlg)
