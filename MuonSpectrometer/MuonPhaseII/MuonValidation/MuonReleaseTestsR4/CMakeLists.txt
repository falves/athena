################################################################################
# Package: MuonReleaseTestsR4
################################################################################

# Declare the package name:
atlas_subdir( MuonReleaseTestsR4 )

atlas_install_scripts( test/*.sh )
